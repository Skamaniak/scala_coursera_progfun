package week01

object lecture04 {
  //Call by value and name

  //Loops indefinitely
  def loop : Boolean = loop

  //x call by value, y call by name
  def and(x: Boolean, y: => Boolean) = if(x) y else false
  def or(x: Boolean, y: => Boolean) = if(x) true else y

  def main(args: Array[String]) = {
    println(and(true, false))
    println(and(true, true))
    //This works because the loop parameter is call-by-name and thus it is not evaluated because the evaluation ends with false on x
    println(and(false, loop))
    //This would repeat infinitely because the second parameter have to be evaluated
    //println(and(true, loop))
  }



}
