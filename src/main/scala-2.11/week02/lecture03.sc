val tolerance = 0.0001
def abs(x: Double) = if (x >= 0) x else -x

def isCloseEnough(x: Double, y: Double) =
    abs((x - y) / x) / x < tolerance

// Returns fixed point of function. Fixed point is that point which fulfills x = f(x)
def fixedPoint(f: Double => Double)(firstGuess: Double) = {
    def iterate(guess: Double): Double = {
        val next = f(guess)
        if(isCloseEnough(guess, next)) next
        else iterate(next)
    }
    iterate(firstGuess)
}

fixedPoint(x => 1 + x / 2)(1)

// The sqrt can be rewritten using the fixedPoint function. sqrt is sqrt(x: Double) = y where y * y = x.
// The equation y * y = x can be changed to y = x / y so we are looking for the fixed point of function y = x / y
def sqrtInfinite(x: Double) = fixedPoint(y => x / y)(1)
// This will not converge, it will oscillate indefinitely. The guess is oscillating between 1.0 and 2.0 values (1.0, 2.0, 1.0, 2.0, ...)
// We need the function to make an average steps in guess improving
//sqrtInfinite(2)

// The function that moves guess only by average between old and new guess
def sqrt(x: Double) = fixedPoint(y => (y + x / y) / 2)(1)
sqrt(2)


// Function that gets function and returns the function with average damping. It takes an old guess as x, then
// applies the function f to x (next round, new guess) and returns average between old and new guess
def averageDamp(f: Double => Double)(x: Double) = (x + f(x)) / 2
def sqrt2(x: Double) = fixedPoint(averageDamp(y => x / y))(1)
sqrt2(2)
sqrt2(4)
