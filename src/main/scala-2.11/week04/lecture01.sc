
//These function are in fact the same
val fun = (x: Int) => x * x

def fun2 = {
    class AnonFun extends Function1[Int, Int] {
        def apply(v1: Int): Int = v1 * v1
    }
    new AnonFun
}

def fun3 = new Function1[Int, Int] {
    def apply(v1: Int): Int = v1 * v1
}

//Also the same thing
val v1 = fun(7)
val v2 = fun.apply(7)


//List example
trait List[T] {
    def isEmpty: Boolean
    def head: T
    def tail: List[T]
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
    def isEmpty = false
    override def toString = head + (if (tail.isEmpty) "" else ", " + tail.toString)
}

class Nil[T] extends List[T] {
    def isEmpty = true
    def head = throw new NoSuchElementException("Nil.head")
    def tail = throw new NoSuchElementException("Nil.tail")
    override def toString = "Empty list"
}

object List{
    def apply[T](): List[T] = new Nil[T]
    def apply[T](x: T): List[T] = new Cons(x, new Nil)
    def apply[T](x: T, y: T): List[T] = new Cons(x, List(y))
    def apply[T](x: T, y: T, z: T): List[T] = new Cons(x, List(y, z))
}

val list1: List[Int] = List()
val list2: List[Int] = List(1)
val list3: List[Int] = List(1, 2)
val list4: List[Int] = List(1, 2, 3)