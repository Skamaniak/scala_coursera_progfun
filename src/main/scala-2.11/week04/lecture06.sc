trait Expr
case class Number(n: Int) extends Expr
case class Sum(e1: Expr, e2: Expr) extends Expr
case class Prod(e1: Expr, e2: Expr) extends Expr
case class Var(n: String) extends Expr

/* case modifier automatically adds object
 * with factory method apply */

val a: Expr = Number.apply(10)
val b: Expr = new Number(10)

/* Case classes also have generated equals and hashCode method using its arguments */
a == b

def eval(e: Expr, env: String => Int): Int = e match {
    case Number(n) => n
    case Var(n) => env(n)
    case Sum(e1, e2) => eval(e1, env) + eval(e2, env)
    case Prod(e1, e2) => eval(e1, env) * eval(e2, env)
    /* If no pattern matches, MatchException is thrown automatically */
}

/* Eval, that should use the minimal number of brackets to print the expression correctly (not verified :-)) */
def show(e: Expr): String = e match {
    case Number(n) => n.toString
    case Var(n) => n
    case Sum(e1, e2) => show(e1) + " + " + show(e2)
    case Prod(Sum(e11, e12), e2) => "(" + show(Sum(e11, e12)) + ") * " + show(e2)
    case Prod(e1, Sum(e21, e22)) =>  show(e1) + " * (" + show(Sum(e21, e22)) + ")"
    case Prod(e1, e2) => show(e1) + " * " + show(e2)
}

val expr = Sum(Number(1), Number(44))
show(expr)
eval(expr, x => throw new Error("No environment provided"))

val exprA = Sum(Prod(Number(2), Var("x")), Var("y"))
val exprB = Prod(Sum(Number(2), Var("x")), Var("y"))
show(exprA)
show(exprB)