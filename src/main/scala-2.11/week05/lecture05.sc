val list = List(1, 2, 3, 4, 5, 6)


def sum(xs: List[Int]) = (0 :: xs) reduceLeft ((x, y) => x + y)
//Can be also represented by
def sum2(xs: List[Int]) = (0 :: xs) reduceLeft (_ + _)
//or
def num3(xs: List[Int]) = (xs foldLeft 0)(_ + _)


//Concat example
def concat[T](xs: List[T], ys: List[T]): List[T] = (xs foldRight ys)(_ :: _)
//If we replace foldRight for foldLeft, there will be concatenation of each element from xs to the accumulator
//List ys. So the operator should be :+ and also this is very inefficient (n * n)
def concat2[T](xs: List[T], ys: List[T]): List[T] = (xs foldLeft ys)(_ :+ _)

//This would not compile because of a type error
//def concat2[T](xs: List[T], ys: List[T]): List[T] = (xs foldLeft ys)(_ :: _)


//Functions rewritten to use foldRight
def mapFun[T, U](xs: List[T], f: T => U): List[U] =
    (xs foldRight List[U]())(f(_) :: _)

def lengthFun[T](xs: List[T]): Int = (xs foldRight 0)((_, y) => y + 1)

lengthFun(list)
mapFun(list, (x: Int) => x + 1)