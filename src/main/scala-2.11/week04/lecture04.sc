//List definition where the Nil implementation is changed to be the
//object not a class (we have only a singleton empty list)
trait List[+T] {
    def isEmpty: Boolean
    def head: T
    def tail: List[T]
    def prepend[U >: T](elem: U): List[U] = new Cons(elem, this)
}
class Cons[T](val head: T, val tail: List[T]) extends List[T] {
    def isEmpty = false
    override def toString = head + (if (tail.isEmpty) "" else ", " + tail.toString)
}
//Nil is now the object but if we have list trait defined as List[T], it means that our list is not covariant
//so the following List[Nothing] <: List[T] is not true even if the Nothing <: T is true.
//We have to make List covariant by adding a + sign before T so the new definition would be List[+T]
//which means that the List is now covariant and List[Nothing] <: List[+T] is true.
object Nil extends List[Nothing] {
    def isEmpty = true
    def head: Nothing = throw new NoSuchElementException("Nil.head")
    def tail: Nothing = throw new NoSuchElementException("Nil.tail")
}

val x: List[String] = Nil


//IntSet definition from one of previous lectures
trait IntSet{
    def isEmpty(): Boolean
}

class Empty extends IntSet {
    def isEmpty() = true
}

class NonEmpty(val x: Int) extends IntSet {
    def isEmpty() = false
}

//Because of the lower bound in prepend, the result type of prepending Empty to List[NonEmpty] is the
//closest super-type of both so the result will be List[IntSet]
def f = (xs: List[NonEmpty], x: Empty) => xs prepend x