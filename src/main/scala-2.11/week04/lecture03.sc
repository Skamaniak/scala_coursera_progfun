trait IntSet{
    def isEmpty(): Boolean
}

class Empty extends IntSet {
    def isEmpty() = true
}

class NonEmpty(val x: Int) extends IntSet {
    def isEmpty() = false
}

val empty = new Empty
val nonEmpty = new NonEmpty(1)
val general: IntSet = empty

//Value x have to be the instance of IntSet or its sub-classes
def upperBound[T <: Empty](x: T) = Unit
//Value x have to be the instance of IntSet or its super-classes
def lowerBound[T >: IntSet](x: T) = Unit
//Value x have to be instance of Empty, IntSet or
def bothBounds[T  >: Empty  <: IntSet](x: T) = Unit

upperBound(empty)
//This will not compile because the nonEmpty and general are not sub-classes of Empty
//upperBound(nonEmpty)
//upperBound(general)

lowerBound(general)
//These will also work because the nearest super-type of both IntSet and Empty is derived (it is IntSet)
//so the argument will be cast to IntSet
lowerBound(empty)
lowerBound(nonEmpty)

//All of these types fits the bound
bothBounds(empty)
bothBounds(nonEmpty)
bothBounds(general)
//This one if out of type bounds
//bothBounds(new Object)
