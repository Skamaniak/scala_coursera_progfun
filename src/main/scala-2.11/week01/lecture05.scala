package week01

object lecture05 {
    //square root (Newton method)


    val precision: Double = 0.001

    def abs(x: Double) = if(x >= 0) x else -x

    def improve(guess: Double, x: Double): Double = (guess + x / guess) / 2

    //The precision have to be proportional to the x number. That is the / x at the end.
    def isGoodEnough(guess: Double, x: Double): Boolean = abs(guess * guess - x) / x < precision

    def sqrtIter(guess: Double, x: Double): Double = {
        if (isGoodEnough(guess, x)) guess
        else sqrtIter(improve(guess, x), x)
    }

    def sqrt(x: Double) = sqrtIter(1.0, x)


    def main(args: Array[String]) =
        println("sqrt 2 = " + sqrt(2.0))
        println("sqrt 4 = " + sqrt(4.0))
        println("sqrt 1e-6 = " + sqrt(1e-6))
        println("sqrt 1e60 = " + sqrt(1e60))
}
