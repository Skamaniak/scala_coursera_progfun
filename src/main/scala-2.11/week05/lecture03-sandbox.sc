trait WhoAmI[T] {
    def tellMe: String
}

implicit object ImString extends WhoAmI[String] {
    def tellMe = "I'm String!"
}

implicit object ImInt extends WhoAmI[Int] {
    def tellMe = "I'm Integer!"
}

implicit object ImBoolean extends WhoAmI[Boolean] {
    def tellMe = "I'm Boolean!"
}

implicit object ImDouble extends WhoAmI[Double] {
    def tellMe = "I'm Double!"
}

def guessWho[T](x: T)(implicit w: WhoAmI[T]) = println(w.tellMe)

guessWho(5)
guessWho(false)
guessWho("Guess ;-)")
guessWho(1.5)

//This would fail, because there is no implicit option for Float
//guessWho(1.5f)

implicit object ImBigDecimal1 extends WhoAmI[BigDecimal] {
    def tellMe = "I'm BigDecimal 1!"
}

implicit object ImBigDecimal2 extends WhoAmI[BigDecimal] {
    def tellMe = "I'm BigDecimal 2!!"
}

//This would also fail due to 2 definitions of WhoAmI for BigDecimal type. Compiler cannot
//decide which one to use so the result is error
//guessWho(BigDecimal(0))