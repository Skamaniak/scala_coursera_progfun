val s1 = Stream(1, 2, 3)
//is the same as (but the compiler can infer the more concrete type
Stream.cons(1, Stream.cons(2, Stream.cons(3, Stream.empty)))

val s2 = (100 to 1000).toStream

//Produces stream
99 #:: s2
//is equivalent to
Stream.cons(99, s2)


def streamRange(lo: Int, hi: Int): Stream[Int] = {
    print(lo + " ")
    if (lo >= hi) Stream.empty
    else Stream.cons(lo, streamRange(lo + 1, hi))
}

streamRange(1, 10).take(3).toList