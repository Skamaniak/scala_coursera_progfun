//Infinite stream of integers starting by some number

def from(n: Int): Stream[Int] = n #:: from(n + 1)
val nats = from(0)

val m4s = nats map (_ * 4)
m4s.take(20).toList


//Erathostenes Sieve
def erathSieve(s: Stream[Int]): Stream[Int] =
    s.head #:: erathSieve(s.tail filter(_ % s.head != 0))


val primes = erathSieve(from(2))
primes.take(10).toList
primes.take(20).toList

//Square root of number
def sqrlStream(x: Double): Stream[Double] = {
    def improve(guess: Double) = (guess + x / guess) / 2
    //Each time the next item from guesses stream is
    // evaluated, it is the more precise guess of the sqrt.
    // It takes the last guess and call the improve on it.

    //Also all of this is stored in lazy val, so all the guesses are not evaluated repeatedly
    lazy val guesses: Stream[Double] = 1 #:: (guesses map improve)
    guesses
}

def isGoodEnough(guess: Double, x: Double) =
    math.abs((guess * guess - x) / x) < 0.0001


sqrlStream(4).take(10).toList
sqrlStream(4).filter(isGoodEnough(_, 4)).take(10).toList





