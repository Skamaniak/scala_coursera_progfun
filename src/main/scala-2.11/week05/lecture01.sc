def last[T](xs: List[T]): T = xs match {
    case List() => throw new Error("init of empty list")
    case List(x) => x
    case y :: ys => last(ys)
}

def init[T](xs: List[T]): List[T] = xs match {
    case List() => throw new Error("init of empty list")
    case List(x) => List()
    case y :: ys => y :: init(ys)
}


// My impl
//def removeAt[T](n: Int, xs: List[T]): List[T] =
//    if(xs.isEmpty && n > 0) throw new Error("Index out of bounds")
//    else if(n == 0) xs.tail
//    else xs.head :: removeAt(n - 1, xs.tail)


def removeAt[T](n: Int, xs: List[T]) = (xs take n) ::: (xs drop n + 1)
removeAt(1, List('a', 'b', 'c', 'd'))

