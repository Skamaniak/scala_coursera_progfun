//The arrow notation is just another way how to express pairs
1 -> 2 == (1, 2)
1 -> 2 -> 3 == ((1,2),3)

val romanNumbers = Map("I" -> 1, "V" -> 5, "X" -> 10)

romanNumbers("V")


//This will throw an NoSuchElementException
//romanNumbers("Nope")

//Get returns option
romanNumbers get "None"
romanNumbers get "V"

//We can pattern match with option type
def translate(romanNumber: String) = romanNumbers.get(romanNumber) match{
    case Some(x) => x
    case None => "missing data"
}

translate("V")
translate("None")


val fruits = List("apple", "pear", "pineapple", "lemon", "banana")

fruits sortWith(_.length < _.length)

//creates map with first letters as a keys
fruits groupBy(_.head)



//Polynomial
class Poly(val terms0: Map[Int, Double]){
    def this(bindings: (Int, Double)*) = this(bindings.toMap)

    val terms = terms0 withDefaultValue 0.0

    def + (other: Poly) = new Poly(terms ++ (other.terms map adjust))
    def adjust(term: (Int, Double)): (Int, Double) = {
        val (exp, coef) = term
        exp -> (coef + terms(exp))
    }

    override def toString = (for ((exp, coef) <- terms.toList.sorted.reverse) yield coef + "x^" + exp).mkString(" + ")
}

val p1 = new Poly(1 -> 2.0, 3 -> 4.0, 5 -> 6.2)
val p2 = new Poly(Map(0 -> 3.0, 3 -> 7.0))
p1 + p2




//Using foldLeft
class Poly2(val terms0: Map[Int, Double]){
    def this(bindings: (Int, Double)*) = this(bindings.toMap)

    val terms = terms0 withDefaultValue 0.0

    def + (other: Poly2) = new Poly((other.terms foldLeft terms)(addTerm))

    def addTerm(terms: Map[Int, Double], term: (Int, Double)) = {
        val (exp, coef) = term
        terms + (exp -> (coef + terms(exp)))
    }
    override def toString = (for ((exp, coef) <- terms.toList.sorted.reverse) yield coef + "x^" + exp).mkString(" + ")
}
val p3 = new Poly2(1 -> 2.0, 3 -> 4.0, 5 -> 6.2)
val p4 = new Poly2(Map(0 -> 3.0, 3 -> 7.0))
p3 + p4