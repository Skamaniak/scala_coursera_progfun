//Vector is a shallow tree with the access complexity of log32(n).
//It is much better to use for bulk operations (fold, map, filter, ...)

//List is better to use if the only accesses are using head and tail operations

//Natural conversions of an array and String to seq
//They are not subclasses of Seq, but they can be converted to it when needed
val xs = Array(1, 2, 4, 8, 16)
xs map (x => x * 2)

val s = "Hello World"
s filter (_.isUpper)


//Range
1 until 5
1 to 5
1 to 10 by 3
6 to 1 by -2

//Operations
val vec = Vector(2, 4, 8, 16, 32)
vec exists (_ > 16)
vec exists (_ > 32)

vec forall (_ % 2 == 0)
vec forall (_ > 2)

val pairs = s zip vec
val (firstVec, secondVec) = pairs.unzip
s flatMap(List('.', _))
vec.sum
vec.max
//combinations
def allCombinations(m: Int, n: Int) =
    (1 to m) flatMap(x => (1 to n) map (y => (x, y)))

allCombinations(5, 2)
def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double =
    (xs zip ys).map(pair => pair._1 * pair._2).sum

//And using pattern matching
def scalarProduct2(xs: Vector[Double], ys: Vector[Double]): Double =
    (xs zip ys).map{case (x, y) => x * y}.sum

//is prime function
def isPrime(n: Int): Boolean = (2 until n) forall (n % _ != 0)
isPrime(2)
isPrime(3)
isPrime(4)
isPrime(9)
isPrime(11)