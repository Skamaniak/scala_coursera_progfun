def msort[T](xs: List[T])(lt: (T, T) => Boolean): List[T] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
        def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
            case (Nil, Nil) => xs
            case (xs1, Nil) => xs1
            case (Nil, ys1) => ys1
            case (x :: xs1, y :: ys1) =>
                if (lt(x, y)) x :: merge(xs1, ys)
                else y :: merge(xs, ys1)
        }
        val (fst, snd) = xs splitAt n
        merge(msort(fst)(lt), msort(snd)(lt))
    }
}

val nums = List(2, -4, 5, 7, 1)
val fruits = List("apple", "pineapple", "orange", "banana")

msort(nums)((x: Int, y: Int) => x < y)
//type of function params can be omitted, compiler can figure the right type from value parameter (fruits). It is a good practice to
//place function parameter as a last one so the type can be figured out.
msort(fruits)((x, y) => x.compareTo(y) < 0)



def msort2[T](xs: List[T])(implicit ord: Ordering[T]): List[T] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
        def merge(xs: List[T], ys: List[T]): List[T] = (xs, ys) match {
            case (Nil, Nil) => xs
            case (xs1, Nil) => xs1
            case (Nil, ys1) => ys1
            case (x :: xs1, y :: ys1) =>
                if (ord.lt(x, y)) x :: merge(xs1, ys)
                else y :: merge(xs, ys1)
        }
        val (fst, snd) = xs splitAt n
        merge(msort2(fst), msort2(snd))
    }
}

msort2(nums)(Ordering.Int)
msort2(fruits)(Ordering.String)
msort2(nums)
msort2(fruits)