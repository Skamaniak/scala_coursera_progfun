import scala.annotation.tailrec

//Non tail recursive function

def sum(f: Int => Int, x: Int, y: Int): Int =
   if(x > y) 0 else f(x) + sum(f, x + 1, y)
sum(x => x, 5, 8)

//tail recursive sum. Annotation only validates compile time that the function is really tail-recursive
def tailSum(f: Int => Int)(a: Int, b: Int): Int = {
    @tailrec
    def loop(a: Int, acc: Int): Int = {
        if (a > b) acc
        else loop(a + 1, f(a) + acc)
    }
    loop(a, 0)
}


tailSum(x => x)(5, 8)