package funsets

object Main extends App {
  import week02.assignment2.funsets.FunSets._
  println(contains(singletonSet(1), 1))

  val s1: Set = x => 10 > x && x > -10
  printSet(s1)
  printSet(map(s1, x => x + x))

  println(forall(s1, x => x > -10))
  println(forall(s1, x => x > -9))

  println(exists(s1, x => x == 0))
  println(exists(s1, x => x == 20))

  printSet(union(singletonSet(1), singletonSet(2)))
  printSet(union(singletonSet(1), singletonSet(1)))

  val s2: Set = x => 10 >= x && x >= 0
  printSet(intersect(x => x > 5, x => x < 10))
  printSet(intersect(s2, singletonSet(5)))

  printSet(diff(s2, singletonSet(5)))
  printSet(diff(s2, singletonSet(5)))

  printSet(filter(s2, x => x <= 5))
}
