package week01

/**
 * Created by Jan on 17. 10. 2015.
 */
object lecture06 {
    /*Lecture 05 cleanup (Blocks and Lexical scope)
        1) The functions that shouldn't be visible from outer scope were moved to sqrt function block
        2) From the functions definitions was removed the redundant x argument definiotion (x is always the same in the sqrt function block)
     */

    def abs(x: Double) = if (x >= 0) x else -x

    def sqrt(x: Double) = {
        val precision: Double = 0.001

        def improve(guess: Double): Double = (guess + x / guess) / 2

        def isGoodEnough(guess: Double): Boolean = abs(guess * guess - x) / x < precision

        def sqrtIter(guess: Double): Double = {
            if (isGoodEnough(guess)) guess
            else sqrtIter(improve(guess))
        }

        sqrtIter(1.0)
    }


    def main(args: Array[String]) =
        println("sqrt 2 = " + sqrt(2.0))
        println("sqrt 4 = " + sqrt(4.0))
        println("sqrt 1e-6 = " + sqrt(1e-6))
        println("sqrt 1e60 = " + sqrt(1e60))
}
