package week01.assignment1

object Main {
    def main(args: Array[String]) {
        println("Pascal's Triangle")
        for (row <- 0 to 10) {
            for (col <- 0 to row)
                print(pascal(col, row) + " ")
            println()
        }
        print(countChange(5, List(1, 2, 5)))
    }

    /**
     * Exercise 1
     */
    def pascal(c: Int, r: Int): Int = if (c == 0 || r == 0 || c == r) 1 else pascal(c - 1, r - 1) + pascal(c, r - 1)

    /**
     * Exercise 2
     */
    def balance(chars: List[Char]): Boolean = {
        def loop(openParenthesis: Int, chars: List[Char]): Boolean =
            if (openParenthesis < 0) false
            else if (chars.isEmpty) openParenthesis == 0
            else if ('(' == chars.head) loop(openParenthesis + 1, chars.tail)
            else if (')' == chars.head) loop(openParenthesis - 1, chars.tail)
            else loop(openParenthesis, chars.tail)
        loop(0, chars)
    }

    /**
     * Exercise 3
     * Hint / pseudocode solution: https://class.coursera.org/progfun-005/forum/thread?thread_id=323#comment-432
     */
    def countChange(money: Int, coins: List[Int]): Int = {
        if (money == 0 && coins.isEmpty) 1
        else if (money < 0) 0
        else if (coins.isEmpty) 0
        else countChange(money, coins.tail) + countChange(money - coins.head, coins)
    }

}
