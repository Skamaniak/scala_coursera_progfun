package sandbox

import java.util.UUID

import akka.actor.{Actor, Props, ActorSystem}

object AkkaObject {
    //Heavy weight actor system
    val system = ActorSystem("mySystem")

    object LogLevel extends Enumeration {
        type LogLevel = Value
        val Error, Warn, Info, Debug = Value
    }

    class ConsoleLogActor extends Actor {

        import LogLevel._

        def logMessage(lvl: LogLevel, message: String) = Unit//println(System.currentTimeMillis() + " " + lvl + " > " + message)

        override def receive: Receive = {
            case (lvl: LogLevel, message: String) => logMessage(lvl, message)
            case x => logMessage(LogLevel.Error, "Unknown log value: " + x)
        }
    }

    class ProducerActor extends Actor {
        var running = false
        val consumer = system.actorOf(Props[ConsoleLogActor], name = "printActor")

        override def receive: Actor.Receive = {
            case "stop" => running = false
            case "run" => running = true; produce()
        }

        def produce(): Unit = {
            while (running) {
                consumer ! (LogLevel.Info, UUID.randomUUID().toString)
            }
        }
    }


    def main(args: Array[String]) {
        val producerActor = system.actorOf(Props[ProducerActor], name = "producer")

        //producerActor ? "run"
        Thread.sleep(1000)
        producerActor ! "stop"
    }
}
