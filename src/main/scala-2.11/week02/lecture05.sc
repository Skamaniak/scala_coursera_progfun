class Rational(x: Int, y: Int){
    //Class represents the rational number in form of numerator / denominator
    def numer = x
    def denom = y

    def neg = new Rational(-numer, denom)
    def add(that: Rational) =
        new Rational(numer * that.denom + that.numer * denom,
            denom * that.denom)
    def sub(that: Rational) = add(that.neg)
    override def toString = numer + "/" + denom
}
val x = new Rational(1, 2)
x.numer
x.denom

val y = new Rational(2, 3)

// (a / b) + (x / y) = (a * y + x * b) / (b * y)
def addRational(r: Rational, s: Rational) =
    new Rational(r.numer * s.denom + s.numer * r.denom,
        r.denom * s.denom)

def makeString(r: Rational) = r.numer + "/" + r.denom

makeString(addRational(x, y))
makeString(x.add(y))

//Call overridden toString
x.add(y)

//Excercise
val x1 = new Rational(1, 3)
val y1 = new Rational(5, 7)
val z1 = new Rational(3, 2)

x1.sub(y1).sub(z1)
