object Email{
    //apply method is optional, just for the example
    def apply(name: String, domain: String) = name + "@" + domain
    def unapply(email: String) = {
        val parts = email split "@"
        if(parts.length == 2) Some(parts(0), parts(1))
        else None
    }

    def isEmpty = false
}

object Domain{
    //Also optional for extractor
    def apply(parts: String*) = parts.reverse.mkString(".")
    def unapply(domain: String) = Some((domain split "\\.").reverse.toList)
}

val em1 = Email("jan.skrabal", "ibacz.cz")
val em2 = Email("jan.skrabal", Domain("cz", "seznam"))
val em3 = Email("john.doe", "node.co.uk")
val em4 = Email("jane.doe", "zoznam.sk")
//

def matchEmail(email: String) = email match {
    case Email(name, "ibacz.cz") => println(email + " - IBA email of " + name)
    case Email(name, Domain("cz" :: other)) => println(email + " - CZ email of " + name)
    case Email(name, Domain(_ :: _ :: _ :: Nil)) => println(email + " - Email with 2 subdomains of " + name)
    case Email(name, domain) => println(email + " - Common email of " + name)
}

matchEmail(em1)
matchEmail(em2)
matchEmail(em3)
matchEmail(em4)
