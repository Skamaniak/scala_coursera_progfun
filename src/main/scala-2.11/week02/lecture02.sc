//sum definition from previous lection
def sum(f: Int => Int, a: Int, b: Int): Int =
    if(a > b) 0 else f(a) + sum(f, a + 1, b)

//Definition of functions using higher-level function sum
def sumInts(a: Int, b: Int) = sum(x => x, a, b)
def sumSquares(a: Int, b: Int) = sum(x => x * x, a, b)
def sumCubes(a: Int, b: Int) = sum(x => x * x * x, a, b)

sumCubesCurr(5, 7) + sumInts(5, 7)



//There is still some redundancy, we can use currying
//sumCurr function returns an partially applied function loop which takes two Ints annd uses function f given in partial application.
//the result thus can be applied on 2 numbers a and b returning the result of sum of application of f to the numbers in range between a and b
def sumCurr(f: Int => Int): (Int, Int) => Int = {
    def loop(a: Int, b: Int): Int = {
        if (a > b) 0
        else f(a) + loop(a + 1, b)
    }
    loop
}

//The arguments are given when the function is called and they are automatically passed to sum. Isn't that cool? :-)
def sumIntsCurr = sumCurr(x => x)
def sumSquaresCurr = sumCurr(x => x * x)
def sumCubesCurr = sumCurr(x => x * x * x)

//We can still use the sumCurr
sumCubesCurr(5, 7) + sumCurr(x => x)(5, 7)


//There is also a syntactical sugar for defining the functions that returns functions
def sumCurr2(f: Int => Int)(a: Int, b: Int): Int = {
    if(a > b) 0 else f(a) + sum(f, a + 1, b)
}

def sumIntsCurr2 = sumCurr(x => x)
def sumSquaresCurr2 = sumCurr(x => x * x)
def sumCubesCurr2 = sumCurr(x => x * x * x)

sumCubesCurr2(5, 7) + sumCurr2(x => x)(5, 7)


//Exercise
def product(f: Int => Int)(a: Int, b: Int): Int =
    if(a > b) 1
    else f(a) * product(f)(a + 1, b)

product(x => x * x)(3, 4)

def fact(n: Int) = product(x => x)(1, n)

fact(0)
fact(1)
fact(5)

//Sum and product generalization
def mapReduce(f: Int => Int, combine: (Int, Int) => Int, default: Int)(a: Int, b: Int): Int =
    if(a > b) default
    else combine(f(a), mapReduce(f, combine, default)(1 + a, b))

def generalProduct1(f: Int => Int)(a: Int, b:Int) = mapReduce(f, (x, y) => x * y, 1)(a, b)

//Definition without a and b
def generalProduct2(f: Int => Int) = mapReduce(f, (x, y) => x * y, 1)(_, _)

generalProduct1(x => x * x)(3, 4)
generalProduct2(x => x * x)(3, 4)

