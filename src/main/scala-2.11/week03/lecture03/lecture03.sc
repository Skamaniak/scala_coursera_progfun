trait List[T] {
    def isEmpty: Boolean
    def head: T
    def tail: List[T]
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
    def isEmpty = false
}

class Nil[T] extends List[T] {
    def isEmpty = true
    def head = throw new NoSuchElementException("Nil.head")
    def tail = throw new NoSuchElementException("Nil.tail")
}


def singleton[T](elem: T) = new Cons[T](elem, new Nil[T])

val l1  = singleton(1)
val l2  = singleton[Int](1)
val l3  = singleton(true)


def nth[T](n: Int, xs: List[T]): T =
    if(xs.isEmpty) throw new IndexOutOfBoundsException
    else if(n == 0) xs.head
    else nth(n - 1, xs.tail)

val list = new Cons(1, new Cons(2, new Cons(3, new Nil)))
val second = nth(1, list)

//Throws IndexOutOfBoundsException as expected
//val nonex = nth(-1, list)
//val nonex = nth(4, list)