def squareList(xs: List[Int]): List[Int] = xs match {
    case Nil => xs
    case y :: ys => (y * y) :: squareList(ys)
}

def squareList2(xs: List[Int]): List[Int] = xs map (x => x * x)

val nums = List(2, -4, 5, 7, 1)
squareList(nums)
squareList2(nums)

nums filter (x => x > 0)
nums filterNot (x => x > 0)
val (meets, doesNotMeet) = nums partition (x => x > 0) //combination of filter and filterNot
nums takeWhile (x => x > 0) //The longest prefix
nums dropWhile (x => x > 0) //Drops the longest prefix
val (dropped, taken) = nums span (x => x > 0) //combination of takeWhile and dropWhile

def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 =>
        val (first, second) = xs span (y => x == y)
        first :: pack(second)
}
val dataList = List("a", "a", "a", "b", "c", "c", "a")
pack(dataList)

//There are two options, but the second is much better
def encode2[T](xs: List[T]): List[(T, Int)] = pack(xs) match {
    case Nil => Nil
    case y :: ys => (y.head, y.length) :: encode(xs)
}

def encode[T](xs: List[T]): List[(T, Int)] = pack(xs) map (ys => (ys.head, ys.length))
encode(dataList)
