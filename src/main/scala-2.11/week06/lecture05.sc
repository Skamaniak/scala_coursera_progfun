//From the last lecture
case class Book(title: String, authors: List[String])
val books: List[Book] = List(
    Book(title = "Structure and Interpretation of Computer Programs",
        authors = List("Abelson, Harald", "Sussman, Gerald J.")),
    Book(title = "Introduction to Functional Programming",
        authors = List("Bird, Richard", "Walder, Phil")),
    Book(title = "Effective Java",
        authors = List("Bloch, Joshua")),
    Book(title = "Java Puzzles",
        authors = List("Bloch, Joshua", "Gaftel, Neal")),
    Book(title = "Programming in Scala",
        authors = List("Odersky, Martin", "Spoon, Lex", "Venners, Bill"))
)

//The for can be translated as a sequence of flatMap, withFilter (lazy filter) and map
/*1)
    for (a <- e1) yield e2
    is the same as
    e1 map (a => e2)
*/
for (b <- books) yield b.title
//is the same as
books map (b => b.title)



/*2) generator and filter
    for (x <- e1 if f; s) yield e2
    is the same as
    for (x <- e1.withFilter(x => f); s) yield e2
*/
for (b <- books if b.title.startsWith("Bird,")) yield b

/*3) 2 generators
    for(x <- e1; y <- e2; s) yield e3
    is the same as
    e1.flatMap(x => for (y <- e2; s) yield e3)

*/
for (b <- books; a <- b.authors) yield a
//is the same as
books flatMap(b => b.authors map (a => a))




//So we can transform this
for (b <- books; a <- b.authors if a startsWith "Bird,") yield b.title
//Into pure form without for expression using the 3 transformations above

//Using 3)
books flatMap (b => for (a <- b.authors if a startsWith "Bird,") yield b.title)
//Using 2)
books flatMap (b => for (a <- b.authors.withFilter(a => a startsWith "Bird,")) yield b.title)
//Using 1)
books flatMap (b => b.authors.withFilter(a => a startsWith "Bird,") map (y => b.title))
// and that's it