def msort(xs: List[Int]): List[Int] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
        def merge(xs: List[Int], ys: List[Int]): List[Int] = xs match {
            case Nil => ys
            case x :: xs1 => ys match {
                case Nil => xs
                case y :: ys1 =>
                    if (x < y) x :: merge(xs1, ys)
                    else y :: merge(xs, ys1)
            }
        }
        val (fst, snd) = xs splitAt n
        merge(msort(fst), msort(snd))
    }
}
val lst = List(1, 5, 8, 7, 6, 124, 52, 1, 2, 3, 88)
msort(lst)

// Another possible non-optimal impl of merge function can be:
//def merge(xs: List[Int], ys: List[Int]): List[Int] =
//    if(xs.isEmpty && ys.isEmpty) List()
//    else if(xs.isEmpty && ys.nonEmpty) ys.head :: merge(xs, ys.tail)
//    else if(xs.nonEmpty && ys.isEmpty) xs.head :: merge(xs.tail, ys)
//    else if(xs.head < ys.head) xs.head :: merge(xs.tail, ys)
//    else ys.head :: merge(xs, ys.tail)

//Pairs and tuples

//Pattern matching test - this is so cool!
val pair = ("answer", 42)
val (label, value) = pair
label
value

val list = List(1, 2, 3)
val List(first, second, third) = list
first
second
third



//Better / cleaner merge sort
def msort2(xs: List[Int]): List[Int] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {
        def merge(xs: List[Int], ys: List[Int]): List[Int] = (xs, ys) match {
            case (Nil, Nil) => xs
            case (xs1, Nil) => xs1
            case (Nil, ys1) => ys1
            case (x :: xs1, y :: ys1) =>
                if (x < y) x :: merge(xs1, ys)
                else y :: merge(xs, ys1)
        }
        val (fst, snd) = xs splitAt n
        merge(msort2(fst), msort2(snd))
    }
}

msort2(lst)