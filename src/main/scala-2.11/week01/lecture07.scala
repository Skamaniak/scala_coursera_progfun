package week01

import scala.annotation.tailrec

/**
 * Created by Jan on 17. 10. 2015.
 */
object lecture07 {

    //Factorial that is not tail recursive (the last action is not the function call, but the multiplication of partial  results. e.g 5 * (4 * (3 * (2 * (1 * 1))))
    def fact(n: Int): Int= if(n == 0) 1 else n * fact(n - 1)

    //This one is tail recursive. The annotation ensures that, if the recursion is not tail-recursive, the compiler (and IDE) would complain
    def tailFact(n: Int): Int= {
        @tailrec
        def factIter(acc: Int, num: Int): Int = if(num == 0) acc else factIter(acc * num, num - 1)
        factIter(1, n)
    }

    def main(args: Array[String]) {
        println(fact(5))

        println(tailFact(5))
    }
}
