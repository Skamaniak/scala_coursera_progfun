val list = List(Some(1), None, Some(3), Some(4), None, None, Some(7))

//Map function to list of options. It will be applied only to Somes, not to Nones
list map (x => x map (_ + 5))

//Filter Nones
list filter(_.isDefined)

//List of options to list of values
list filter(_.isDefined) map (_.get)

val ov: Option[Int] = Some(5)
val on: Option[Int] = None

ov.getOrElse(-1)
on.getOrElse(-1)




def op(acc: Int, f: (Int, Int) => Int)(s: Seq[Option[Int]]) =
    ((s.filter(_.isDefined) map (_.get)) foldLeft acc) (f)

def sum = op(0, _ + _) _
def count = op(0, (x, y) => x + 1) _
def prod = op(1, _ * _) _

val s = sum(list)
val p = prod(list)
val c = count(list)