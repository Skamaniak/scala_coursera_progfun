//From previous lecture
def isPrime(n: Int): Boolean = (2 until n) forall (n % _ != 0)


val n = 7

//Solution without for
val pairs = ((1 until n) map (i =>
    (1 until i) map (j => (i, j)))).flatten

//It is the same as:
val pairs2 = (1 until n) flatMap (i =>
    (1 until i) map (j => (i, j)))
//Because the xs flatMap f is the same as: (xs map f).flatten

pairs filter (pair => isPrime(pair._1 + pair._2))

//Solution with for
for {
    i <- 1 until n
    j <- 1 until i
    if isPrime(i + j)
} yield (i, j)

//Excercise
def scalarProduct(xs: List[Double], ys: List[Double]) : Double =
    (for {(x, y) <- xs zip ys} yield x * y).sum