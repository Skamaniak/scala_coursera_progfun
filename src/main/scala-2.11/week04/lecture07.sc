val list1 = List("red", "green", "blue")

val list2 = List(1, 2, 3)
val list3 = 1 :: (2 :: (3 :: Nil))
list2 == list3

// Every operator in scala that ends with : associates to the right so we can just write
val list4 = 1 :: 2 :: 3 :: Nil
list3 == list4


// All operators in scala that ends with : are invoked on the RIGHT operand!
// Below is the simple example where you can see that the user defined operator
// :: is invoked on class c2 even if you write c1 :: c2
class Class1
class Class2 {
    def :: (c1: Class1) = null
}

val c1 = new Class1()
val c2 = new Class2()

c1 :: c2
//This would fail because the c1 do not implement function ::
// c2 :: c1


//If you have
val list5 = 1 :: 2 :: 3 :: 4 :: Nil
//It is rewritten to this
val list6 = Nil.::(4).::(3).::(2).::(1)
list5 == list6

def insert(x: Int, xs: List[Int]): List[Int] = xs match {
    case List() => List(x)
    case y :: ys => if(x <= y) x :: xs else y :: insert(x, ys)
}

def isort(xs: List[Int]): List[Int] = xs match {
    case List() => List()
    case y :: ys => insert(y, isort(ys))
}

val list7 = List(1, 5, 2, 7, 9, 6, 3, 4, 5, 8, 8, 7, 1, 0)
isort(list7)