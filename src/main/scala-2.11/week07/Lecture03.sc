
//def x = expr
//The x is evaluated only if someones calls it and it is evaluated repeatedly (every time it's used)

//lazy val x = expr
//Lazy values are also evaluated on demand, but only once. Each next call gets the evaluated value


def expr = {
    val x = {print("x"); 1}
    lazy val y = {print("y"); 2}
    def z = {print("z"); 3}

    z + y + x + z + y + x
}

expr
//It prints xzyz because the
// x is evaluated immediately and only once
// y is evaluated on demand and only once
// z is evaluated every time it is called


//Just a note (it was covered by previous lectures)
val l = List(1, 2, 3, 4)

l(1)
// is the same as
l.apply(1)