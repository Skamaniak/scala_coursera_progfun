class Rational(x: Int, y: Int){
    require(y != 0, "denominator must be nonzero")
    //Alternative constructor
    def this(x: Int) = this(x, 1)
    private def gcd(a: Int, b: Int): Int = if(b == 0) a else gcd(b, a % b)
    private val g = gcd(x, y)
    def numer = x / g
    def denom = y / g
    def unary_- = new Rational(-numer, denom)
    def + (that: Rational) =
        new Rational(numer * that.denom + that.numer * denom,
            denom * that.denom)
    def - (that: Rational) = this + -that
    def < (that: Rational) = numer * that.denom < that.numer * denom
    def * (that: Rational) = new Rational(numer * that.numer, denom * that.denom)
    def max(that: Rational) = if(this < that) that else this
    override def toString = numer + "/" + denom
}

val x = new Rational(1, 3)
val y = new Rational(5, 7)
val z = new Rational(3, 2)
-x
x.numer
y.denom
x - y - z
y + y
x < y
x max y

//Precedence of operators are given by first character of an user-defined operator.
// The priorities are the same as in math so * has higher priority as +
x * x + y * y
(x * x) + (y * y)

