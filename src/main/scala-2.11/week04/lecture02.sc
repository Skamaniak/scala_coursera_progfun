import java.util.NoSuchElementException

abstract class MyBoolean {
    def ifThenElse[T](t: => T, e: => T): T
    def &&(x: => MyBoolean): MyBoolean = ifThenElse(x, new myFalse)
    def ||(x: => MyBoolean): MyBoolean = ifThenElse(x, new myTrue)
    def unary_! : MyBoolean = ifThenElse(new myFalse, new myTrue)

    //...

    def <(x: => MyBoolean): MyBoolean = ifThenElse(new myFalse, x)
}

class myFalse extends MyBoolean {
    def ifThenElse[T](t: => T, e: => T) = e
}

class myTrue extends MyBoolean {
    def ifThenElse[T](t: => T, e: => T) = t
}


//Peano numbers
abstract class Nat {
    def isZero: Boolean
    def predecessor: Nat
    def successor: Nat = new Succ(this)
    def +(that: Nat): Nat
    def -(that: Nat): Nat
}

object Zero extends Nat(){
    def isZero: Boolean = true
    def predecessor: Nat = throw new Error("Zero.predecessor")
    def + (that: Nat): Nat = that
    def - (that: Nat): Nat = if(that.isZero) this else throw new Error("Negative number")
}

class Succ(val predecessor: Nat) extends Nat{
    def isZero: Boolean = false
    def +(that: Nat): Nat = new Succ(predecessor + that)
    def -(that: Nat): Nat = if(that.isZero) this else predecessor - that.predecessor
}