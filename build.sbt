name := "CourseraScala"

version := "1.0"

scalaVersion := "2.11.7"

javaOptions += "-Xmx2G"
scalacOptions ++= Seq("-deprecation", "-feature")

libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"
libraryDependencies += "junit" % "junit" % "4.10" % "test"
libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.2"

//This is only for sandbox purposes
libraryDependencies +="com.typesafe.akka" %% "akka-actor" % "2.3.14"
